package com.custom.blutoothtext

import android.content.IntentFilter
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    lateinit var intentFilter: IntentFilter
    lateinit var networkChangeReceiver: NetworkChangeReceiver

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        networkChangeReceiver = NetworkChangeReceiver()
        intentFilter = IntentFilter()
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE")
        registerReceiver(networkChangeReceiver, intentFilter)
        networkChangeReceiver.setCallBackNetWork(object : NetworkChangeReceiver.CallBackNetWork {
            override fun callBack(str: String?) {
                showNetWork.text = str
            }
        })
    }
}
